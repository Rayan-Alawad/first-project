import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { userInfo } from 'os';
import { browser } from 'protractor';
import { LocalStorageService } from '../local-storage.service';


@Component({
  selector: 'app-editing',
  templateUrl: './editing.page.html',
  styleUrls: ['./editing.page.scss'],
})
export class EditingPage implements OnInit {
  sub='';
  str1;str2;str: string='';
  thename;

  public User = {Name: this.sub, Password: this.str}

  constructor(private router: Router, private localStorageService:LocalStorageService) {
    this.sub=this.router.getCurrentNavigation().extras.state.event;
     this.thename = this.localStorageService.getItem(this.sub);
     this.str1 = this.thename.substring(this.thename.indexOf(":") + 1);
     this.str2 =this.str1.substring(this.str1.indexOf(":") + 1);
     this.str =this.str2.substring(1,this.str2.indexOf("}"));


  }
Removedite(){
  this.localStorageService.removeItem(this.sub)
}

  save()
  {
    this.User.Name = this.sub;
    this.User.Password = this.str;
    this.localStorageService.setItem(this.sub,  JSON.stringify(this.User))

  }

  ngOnInit() {
  }

}
