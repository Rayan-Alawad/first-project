import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../local-storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterPage } from '../register/register.page';
import { MembersPage } from '../members/members.page';
import { JsonPipe } from '@angular/common';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public Name: string = '';
  public Password: string = '';
  public User = {Name: this.Name, Password: this.Password}
  loginStatus = 'Not logged yet';

   constructor(private routers:Router,private localStorageService: LocalStorageService) { }

  ngOnInit() {
  }

  onLogin(logN,logP) {

   this.User.Name=logN;
   this.User.Password=logP;
 if (JSON.stringify(this.User)===this.localStorageService.getItem(this.User.Name)){

     this.routers.navigateByUrl('/members')
   }else{
    return this.loginStatus = 'wrong name or password';
   }

  }

  onNewRegister(){
    this.routers.navigateByUrl('/register');


  }
}
