import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from './../local-storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public Name: string = '';
  public Password: string = '';
  public User = {Name: this.Name, Password: this.Password}



  registerStatus='complete the register'

  constructor(private router: Router, private localStorageService: LocalStorageService) { }

  ngOnInit() {
  }

  onAddUser(){
    if(this.Name.trim().length <=0||this.Password.trim().length<=0){
      return this.registerStatus='Invalid Input'
    }
    this.User.Name = this.Name;
    this.User.Password = this.Password;
    this.localStorageService.setItem(this.Name,  JSON.stringify(this.User))
    this.router.navigateByUrl('/login')
  }

}
